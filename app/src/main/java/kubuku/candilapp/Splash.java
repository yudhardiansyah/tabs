package kubuku.candilapp;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class Splash extends AppCompatActivity {
    Context context = this;
    ImageView logoCandil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        logoCandil = (ImageView) findViewById(R.id.logo_candil);

        new Thread(new Runnable () {
            @Override
            public void run() {
                SystemClock.sleep(3000);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(context, Welcome.class);
                        startActivity(intent);
                    }
                });
            }
        }).start();
    }
}
