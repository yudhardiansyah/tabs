package kubuku.candilapp;

public class Book {

    private String name;
    private int numOfAuthor;
    private int thumbnail;

    public Book() {
    }

    public Book(String name, int numOfAuthor, int thumbnail) {
        this.name = name;
        this.numOfAuthor = numOfAuthor;
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfAuhor() {
        return numOfAuthor;
    }

    public void setNumOfAuthor(int numOfAuthor) {
        this.numOfAuthor = numOfAuthor;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

}
